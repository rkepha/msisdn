from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from validatorApp import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', include('validatorApp.urls')),

]


