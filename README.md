### Create Virtual Evironment
* 'python3 -m venv <NameOfEnvironment>'
* 'source <>/bin/activate to activate'
* pip install -r requirements.txt  to install all dependencies

### TO RUN
* Navigate to validator
* `./manage.py runserver` or `python3.6 manage.py runserver` to run the application

### API endpoint

* localhost:8000/msisdn/<msisdnCharacters>