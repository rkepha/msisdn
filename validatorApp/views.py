# from django.shortcuts import render,redirect
from django.http import Http404, HttpResponse
from django.conf import settings
from validatorApp.msisdnscript import which_network
import json

def validate(request, msisdn):

    network = which_network(msisdn)

    response_data= {
        "msisdn":msisdn,
    }

    if network==0:
        response_data['network_id'] = 1
   
    elif network==1:
        response_data['network_id'] = 2

    elif network==2:
        response_data['network_id'] = 3

    elif network==3:
        response_data['network_id'] = 4

    else:
        response_data['error'] = "no network_id found"

    return HttpResponse(json.dumps(response_data), content_type="application/json")