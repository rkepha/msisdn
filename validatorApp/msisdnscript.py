import re

safaricom = [     
        '254701', '254702', '254703', '254704', '254705', '254706', '254707', '254708',
        '254710', '254711', '254712', '254713', '254714', '254715', '254716', '254717', '254718','254719',
        '254720', '254721', '254722', '254723', '254724', '254725', '254726', '254727', '254728','254729',
        '254790', '254791', '254792',
        '254740', '254741', '254742', '254743', '254744', '254745', '254746', '254747', '254748', '254749',
        '254757', '254758', '254759', 
]

airtel = [
        '254730', '254731', '254732', '254733', '254734', '254735', '254736', '254737', '254738', '254739',
        '254750', '254751', '254752', '254753', '254754', '254755', '254756', 
        '254785', '254786', '254787', '254788', '254789',  '254780'
]

orange = [
        '254770', '254771', '254772', '254773', '254774', '254775', '254776','254778'
]

equitel = [
        '254763', '254764'
]


# operators lists
operators = [safaricom, airtel, orange, equitel]

international_mode = False

country_dial_code = '254'

# identify the network id
def which_network(msisdn):

    # sanitize the msisdn
    re.sub("[^0-9]", "", msisdn)

    

    # remove the leading zero
    if msisdn[:1] == "0" and len(msisdn) == 10:
        msisdn = msisdn[1:]
    
    # If the # is less than the country's #
    if len(msisdn)<=9 and len(msisdn)>0:
        msisdn = country_dial_code + msisdn

    if international_mode:
        msisdn = "+" + msisdn
        
    # get first 6 digits of msisdn
    prefix = msisdn[:6]

    for operator in operators:
        if prefix in operator:
            return operators.index(operator)
