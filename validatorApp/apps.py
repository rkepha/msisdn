from django.apps import AppConfig


class ValidatorappConfig(AppConfig):
    name = 'validatorApp'
